# Logitech 27MHz Keyboard Encryption Setup

A tool for enabling encryption on the 27 MHz wireless connection used by some (somewhat older) Logitech wireless keyboards.

## Usage

Plug in the USB receiver for the keyboard, open a terminal and as root run `lg-27MHz-keyboard-encryption-setup` and follow the instructions. Note you will not be able to use the keyboard as keyboard while the encryption is being setup.

## Supported platforms

The lg-27MHz-keyboard-encryption-setup tool was developed and tested on Linux, it uses libusb-1.0 for USB abstraction so it may work on other platforms too, but this has not been tested.

## My Logitech 27MHz wireless keyboard has stopped working?

The keyboard may stop working when it was setup to use encryption and the encryption key in the receiver somehow gets lost, this can happen for example when the receiver is not used for a long time (it seems to use battery backed RAM); or when the keyboard gets paired to another receiver.

Newer linux kernels will print the following messages when this is the case:

```
[16689.643387] logitech-hidpp-device 0003:046D:0056.0013: Error the keyboards wireless encryption key has been lost, your keyboard will not work unless you re-configure encryption.
```

This can be fixed by running lg-27MHz-keyboard-encryption-setup to set a new key. Alternatively (*not recommended!*) the encryption key can be cleared, disabling encryption, by entering the wrong encryption key on the keyboard when asked the enter the new key.

## Manually disabling encryption to fix the keyboard not working

Here are step by step instructions for clearing the encryption key (disabling encryption) without needing to use the lg-27MHz-keyboard-encryption-setup tool. These should work to restore a non working connection to a Logitech 27MHz wireless keyboard regardless of which OS you are using:

1. Press left-Ctrl + left-Alt + F12 on the keyboard to start encryption setup
2. Press the connect button on the receiver
3. Press the connect button on the keyboard
4. The keyboard now expects you to enter a new encryption key consisting of 16 characters. Press the 'A' key 16 times, this is an invalid key, which will result in encryption getting disabled, restoring the connection.

Note steps 1 - 3 need to be done relatively quickly otherwise the keyboard will timeout from its "encryption setup mode". During step 4. the keyboard will send a '*' press for each time you press the 'A' key, this is normal.
