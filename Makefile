PREFIX  = /usr/local
CFLAGS  = -g -Wall -O2
LDFLAGS = -g
OBJS    = lg-27MHz-keyboard-encryption-setup.o
TARGET  = lg-27MHz-keyboard-encryption-setup

$(TARGET): $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $^ `pkg-config --libs libusb-1.0`

%.o: %.c
	$(CC) $(CFLAGS) `pkg-config --cflags libusb-1.0` -o $@ -c $<

install: $(TARGET)
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	install -p -m 755 $(TARGET) $(DESTDIR)$(PREFIX)/bin
	
clean:
	rm -f $(OBJS) $(TARGET) *~
