// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Logitech 27MHz Keyboard Encryption Setup tool.
 *
 * Copyright (C) 2020-2021 Hans de Goede <hdegoede@redhat.com>
 */
#include <stdio.h>
#include <string.h>
#include <libusb.h>
#include <linux/hid.h>

#define REPORT_ID_HIDPP_SHORT			0x10
#define REPORT_ID_HIDPP_LONG			0x11

#define HIDPP_REPORT_SHORT_LENGTH		7
#define HIDPP_REPORT_LONG_LENGTH		20

#define KBD_INTERFACE_NUM			0
#define KBD_INPUT_ENDPOINT			0x81

#define HIDPP_INTERFACE_NUM			1
#define HIDPP_INPUT_ENDPOINT			0x82

#define HIDPP_SET_SHORT_REGISTER		0x80
#define HIDPP_GET_SHORT_REGISTER		0x81
#define HIDPP_SET_LONG_REGISTER			0x82
#define HIDPP_GET_LONG_REGISTER			0x83

#define HIDPP_KBD_INDEX				0x03
#define HIDPP_RECEIVER_INDEX			0xff

#define HIDPP_REG_CONNECTION_STATE		0x02
#define HIDPP_FAKE_DEVICE_ARRIVAL		0x02

#define HIDPP_REG_RX0_RADIO_CONTROL		0x70
#define HIDPP_SET_WAIT_LOCK			0x02

#define REPORT_TYPE_USER_INTERFACE_EVENT	0x08
#define USER_INTERFACE_EVENT_USER_ACT_FLAG	0x02

#define REPORT_TYPE_NOTIF_DEVICE_PAIRED		0x41
/* report type 0x47 is used to report the 27 MHz kbd encryption key */
#define REPORT_TYPE_27MHZ_KBD_KEY		0x47

#define SHORT_TIMEOUT				1000  /* ms */
#define LONG_TIMEOUT				60000 /* ms */

/*
 * If the encryption key is lost we will get a lot of packets from the
 * keyboard complaining about this while the user presses Ctrl + Alt + F12
 * and the connect key, so just use lots of retries.
 */
#define WAIT_FOR_TRIES				99

const int encrypt_setup_done_pattern[HIDPP_REPORT_SHORT_LENGTH] = {
	REPORT_ID_HIDPP_SHORT,
	HIDPP_KBD_INDEX,
	REPORT_TYPE_USER_INTERFACE_EVENT,
	0,
	-1,
	-1,
	-1
};

const int kbd_connection_report_pattern[HIDPP_REPORT_SHORT_LENGTH] = {
	REPORT_ID_HIDPP_SHORT,
	HIDPP_KBD_INDEX,
	REPORT_TYPE_NOTIF_DEVICE_PAIRED,
	0x02, /* 27 MHz connection type */
	-1,
	-1,
	-1
};

const int asterisk_pattern[8] = {
	0x00, 0x00, 0x55, -1, 0x00, 0x00, 0x00, 0x00
};

const uint16_t lg_27m_recv_vid_pids[][2] = {
	{ 0x46d, 0xc513 },
	{ 0x46d, 0xc517 },
	{},
};

libusb_device_handle *usbdev = NULL;

int hidpp_receive(uint8_t *buf, int timeout)
{
	int ret, transferred;

	ret = libusb_interrupt_transfer(usbdev, HIDPP_INPUT_ENDPOINT,
					buf, HIDPP_REPORT_LONG_LENGTH,
					&transferred, timeout);
	if (ret) {
		/* Do not log an error for short timeouts */
		if (!(timeout < 100 && ret ==  LIBUSB_ERROR_TIMEOUT))
			fprintf(stderr, "\nError receiving data: %s.\n", libusb_strerror(ret));
		return ret;
	}

	switch (buf[0]) {
	case REPORT_ID_HIDPP_SHORT:
		if (transferred != HIDPP_REPORT_SHORT_LENGTH) {
			fprintf(stderr, "\nError receiving data: expected %d bytes, received %d bytes.\n",
				HIDPP_REPORT_SHORT_LENGTH, transferred);
			return 1;
		}
		break;
	case REPORT_ID_HIDPP_LONG:
		if (transferred != HIDPP_REPORT_LONG_LENGTH) {
			fprintf(stderr, "\nError receiving data: expected %d bytes, received %d bytes.\n",
				HIDPP_REPORT_LONG_LENGTH, transferred);
			return 1;
		}
		break;
	default:
		fprintf(stderr, "\nError receiving data: Unexpected report-id: 0x%02x.\n",
			buf[0]);
		return 1;
	}

	return 0;
}

int hidpp_buf_matches_pattern(const uint8_t *buf, const int *expected, int len)
{
	int i;

	for (i = 0; i < len; i++) {
		if (expected[i] == -1)
			continue;

		if (buf[i] != expected[i])
			break;
	}

	return i == len;
}

int hidpp_wait_for(uint8_t *buf, const int *expected, int timeout, int tries)
{
	int ret;

	while (tries--) {
		ret = hidpp_receive(buf, timeout);
		if (ret)
			return ret;

		if (hidpp_buf_matches_pattern(buf, encrypt_setup_done_pattern, HIDPP_REPORT_SHORT_LENGTH)) {
			fprintf(stderr, "Error keyboard encryption setup timed out.\n");
			fprintf(stderr, "The keyboard times out if there are more then 15 seconds between pressing Ctrl + Alt + F12 and pressing the connect button on the bottom of the keyboard.\n");
			fprintf(stderr, "Please try again.\n");
			return 1;
		}

		if (hidpp_buf_matches_pattern(buf, expected, HIDPP_REPORT_SHORT_LENGTH))
			return 0;

#if 0
		fprintf(stderr, "Unexpected reply %02x %02x %02x %02x %02x %02x %02x packet.\n",
			buf[0], buf[1], buf[2], buf[3],
			buf[4], buf[5], buf[6]);
#endif
	}

	fprintf(stderr, "Error did not receive the expected %02x %02x %02x %02x %02x %02x %02x packet.\n",
		expected[0], expected[1], expected[2], expected[3],
		expected[4], expected[5], expected[6]);

	return 1;
}

int hidpp_set_register(uint8_t device_idx, uint8_t reg_addr,
		       uint8_t p0, uint8_t p1, uint8_t p2)
{
	uint8_t buf[HIDPP_REPORT_SHORT_LENGTH];
	uint8_t recv_buf[HIDPP_REPORT_LONG_LENGTH];
	int expected[HIDPP_REPORT_SHORT_LENGTH];
	int ret;

	buf[0] = REPORT_ID_HIDPP_SHORT;
	buf[1] = device_idx;
	buf[2] = HIDPP_SET_SHORT_REGISTER;
	buf[3] = reg_addr;
	buf[4] = p0;
	buf[5] = p1;
	buf[6] = p2;

	ret = libusb_control_transfer(usbdev,
		LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_RECIPIENT_INTERFACE,
		HID_REQ_SET_REPORT,
		0x0200 | REPORT_ID_HIDPP_SHORT, HIDPP_INTERFACE_NUM,
		buf, sizeof(buf), SHORT_TIMEOUT);
		
	if (ret != sizeof(buf)) {
		fprintf(stderr, "Error setting short reg 0x%02x: %s.\n",
			reg_addr, libusb_strerror(ret));
		return ret;
	}

	expected[0] = REPORT_ID_HIDPP_SHORT;
	expected[1] = device_idx;
	expected[2] = HIDPP_SET_SHORT_REGISTER;
	expected[3] = reg_addr;
	expected[4] = 0;
	expected[5] = 0;
	expected[6] = 0;

	return hidpp_wait_for(recv_buf, expected, SHORT_TIMEOUT, WAIT_FOR_TRIES);
}

int poll_kbd_for_asterisk(int *i)
{
	int ret, transferred;
	uint8_t buf[8];

	if (*i >= 16)
		return 0;

	ret = libusb_interrupt_transfer(usbdev, KBD_INPUT_ENDPOINT,
					buf, 8, &transferred, 10);
	if (ret && ret != LIBUSB_ERROR_TIMEOUT) {
		fprintf(stderr, "\nError receiving data: %s.\n", libusb_strerror(ret));
		return ret;
	}

	if (ret == 0 && transferred == 8 && hidpp_buf_matches_pattern(buf, asterisk_pattern, 8)) {
		if (*i == 3 || *i == 7 || *i == 11) {
			printf("* ");
			fflush(stdout);
		} else {
			printf("*");
			fflush(stdout);
		}
		(*i)++;
	}

	return 0;
}

int poll_hidpp_for_report(int *success, int *done)
{
	uint8_t buf[HIDPP_REPORT_LONG_LENGTH];
	int ret;

	ret = hidpp_receive(buf, 10);
	if (ret && ret != LIBUSB_ERROR_TIMEOUT) {
		return ret;
	}
	if (ret == 0 &&
	    hidpp_buf_matches_pattern(buf, kbd_connection_report_pattern, HIDPP_REPORT_SHORT_LENGTH) &&
	    buf[4] & 0x80) {
		*success = 1;
	}
	if (ret == 0 &&
	    hidpp_buf_matches_pattern(buf, encrypt_setup_done_pattern, HIDPP_REPORT_SHORT_LENGTH)) {
		*done = 1;
	}

	return 0;
}

int main(int argc, char *argv[])
{
	uint8_t buf[HIDPP_REPORT_LONG_LENGTH];
	int expected[HIDPP_REPORT_SHORT_LENGTH];
	int i, ret, success, done, force = 0, rx_channel = 0;

	if (argc == 2 && strcmp(argv[1], "--force") == 0) {
		force = 1;
	} else if (argc > 1) {
		fprintf(stderr, "Usage %s [--force]\n", argv[0]);
		return 1;
	}

	libusb_init(NULL);

	for (i = 0; lg_27m_recv_vid_pids[i][0] && usbdev == NULL; i++) {
		usbdev = libusb_open_device_with_vid_pid(NULL,
						lg_27m_recv_vid_pids[i][0],
						lg_27m_recv_vid_pids[i][1]);
	}
	if (usbdev == NULL) {
		fprintf(stderr, "Error could not find a supported Logitech 27 MHz receiver.\n");
		fprintf(stderr, "Note this program requires low-level USB access, which typically requires running it as root.\n");
		ret = 1;
		goto exit_no_usbdev;
	}

	ret = libusb_set_auto_detach_kernel_driver(usbdev, 1);
	if (ret) {
		fprintf(stderr, "Error could not set auto-detach-kernel-driver: %s.\n",
			libusb_strerror(ret));
		goto exit;
	}

	ret = libusb_claim_interface(usbdev, KBD_INTERFACE_NUM);
	if (ret) {
		fprintf(stderr, "Error could not claim keyboard interface: %s.\n",
			libusb_strerror(ret));
		goto exit;
	}

	ret = libusb_claim_interface(usbdev, HIDPP_INTERFACE_NUM);
	if (ret) {
		fprintf(stderr, "Error could not claim HID++ interface: %s.\n",
			libusb_strerror(ret));
		goto exit;
	}

	/* Request report of connected devices */
	ret = hidpp_set_register(HIDPP_RECEIVER_INDEX,
				 HIDPP_REG_CONNECTION_STATE,
				 HIDPP_FAKE_DEVICE_ARRIVAL, 0, 0);
	if (ret)
		goto exit;

	/* Check keyboard device-connection report */
	ret = hidpp_wait_for(buf, kbd_connection_report_pattern, SHORT_TIMEOUT, WAIT_FOR_TRIES);
	if (ret)
		goto exit;

	switch (buf[4] & 0x03) {
	case 0:
		fprintf(stderr, "Error: No RX channel assigned to keyboard, not paired?.\n");
		ret = 1;
		goto exit;
	case 1:
		rx_channel = 0;
		break;
	case 2:
		rx_channel = 1;
		break;
	case 3:
		fprintf(stderr, "Error: Unexpected RX channel (2) assigned to keyboard.\n");
		ret = 1;
		goto exit;		
	}

	if (buf[4] & 0x80) {
		printf("Keyboard connection is already encrypted.\n");
		if (force) {
			printf("--force specified, continuing.\n");
		} else {
			printf("Specify --force to redo encryption setup.\n");
			goto exit;
		}
	} else {
		printf("Keyboard connection is currently not encrypted.\n");
	}

	printf("\n");
	printf("Step 1: Press left-Ctrl + left-Alt + F12 to start encryption setup.\n");

	/* Wait for keyboard to acknowledge left-Ctrl + left-Alt + F12 press */
	expected[0] = REPORT_ID_HIDPP_SHORT;
	expected[1] = HIDPP_KBD_INDEX;
	expected[2] = REPORT_TYPE_USER_INTERFACE_EVENT;
	expected[3] = USER_INTERFACE_EVENT_USER_ACT_FLAG;
	expected[4] = -1; /* Don't care */
	expected[5] = -1;
	expected[6] = 0;
	ret = hidpp_wait_for(buf, expected, LONG_TIMEOUT, WAIT_FOR_TRIES);
	if (ret)
		goto exit;

	/* Start the repairing sequence on the receiver side */
	ret = hidpp_set_register(HIDPP_RECEIVER_INDEX,
				 HIDPP_REG_RX0_RADIO_CONTROL + rx_channel,
				 HIDPP_SET_WAIT_LOCK, 0, 0);
	if (ret)
		goto exit;

	printf("Step 2: Press the connect button on the bottom of the keyboard.\n");

	/* Wait for receiver to send the key the user needs to enter on the kbd */
	expected[0] = REPORT_ID_HIDPP_LONG;
	expected[1] = HIDPP_KBD_INDEX;
	expected[2] = REPORT_TYPE_27MHZ_KBD_KEY;
	expected[3] = 16; /* 16 bytes key length? */
	expected[4] = -1; /* Don't care */
	expected[5] = -1;
	expected[6] = -1;
	ret = hidpp_wait_for(buf, expected, LONG_TIMEOUT, WAIT_FOR_TRIES);
	if (ret)
		goto exit;

	printf("Step 3: Enter the following key on the keyboard:\n");
	for (i = 0; i < 16; i++) {
		if (i == 4 || i == 8 || i == 12)
			printf(" %c", buf[i + 4]);
		else
			printf("%c", buf[i + 4]);
	}
	printf("\n");

	/*
	 * This code could be rewritten to use the libusb async API, but this
	 * little utility is not very performanc-critical, so we just keep it
	 * simple and interleave polling both endpoints at 10 ms intervals.
	 */
	i = 0; success = 0; done = 0;
	while (done == 0) {
		ret = poll_kbd_for_asterisk(&i);
		if (ret)
			goto exit;

		ret = poll_hidpp_for_report(&success, &done);
		if (ret)
			goto exit;
	}
	printf("\n");

	if (success) {
		printf("Keyboard connection encryption successfully enabled.\n");
	} else {
		fprintf(stderr, "Keyboard connection encryption could not be enabled.\n");
		fprintf(stderr, "Possibly you were too slow with entering the key; or you made an error while entering the key.\n");
		fprintf(stderr, "Please try again.\n");
		ret = 1;
		goto exit;
	}

exit:
	libusb_release_interface(usbdev, HIDPP_INTERFACE_NUM);
	libusb_release_interface(usbdev, KBD_INTERFACE_NUM);
	libusb_close(usbdev);
exit_no_usbdev:
	libusb_exit(NULL);
	return ret ? 1 : 0;
}
